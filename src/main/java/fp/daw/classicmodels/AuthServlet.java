package fp.daw.classicmodels;

import java.io.IOException;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.sql.DataSource;

@WebServlet("/login")
public class AuthServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	
	private String firstName = "";
	private String lastName = "";
	private String codigoConfirmacion = "";

	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		HttpSession session = request.getSession();
		if (session.getAttribute("user") != null)
			response.sendRedirect("catalogo.jsp");
		else {
			String email = request.getParameter("email");
			String password = request.getParameter("password");
			if (email == null || password == null)
				response.sendRedirect("login.jsp");
			else {
				switch (validateCredentials(email, password, session)) {
				case 0:
					response.sendRedirect("catalogo.jsp");
					break;
				case 1:
					response.sendRedirect("login.jsp?status=1");
					break;
				case 2:
					response.sendRedirect("login.jsp?status=2");
					break;
				case 3:
					response.sendRedirect("postregistro.jsp?nombre=" + firstName + "&apellidos=" + lastName + "&email=" + email);
					break;
				}
			}
		}
	}

	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		doGet(request, response);
	}

	private int validateCredentials(String email, String password, HttpSession session) {
		Connection con = null;
		Statement stm = null;
		ResultSet rs = null;
		int result = 0;
		try {
			Context context = new InitialContext();
			DataSource ds = (DataSource) context.lookup("java:comp/env/jdbc/classicmodels");
			con = ds.getConnection();
			stm = con.createStatement();
			String query = "select lastName, firstName, password, confirmationCode from signups where customerEmail = '" + email + "'";
			rs = stm.executeQuery(query);
			if (rs.next() == false) {
				result = 1;
			} else {
				String hashedPassword = rs.getString("password");
				if (HashUtils.validateBase64Hash(password, hashedPassword)) {
					firstName = rs.getString("firstName");
					lastName = rs.getString("lastName") ;
					codigoConfirmacion = rs.getString("confirmationCode");
					if (codigoConfirmacion == null) {
						String name = lastName + ", " + firstName;
						session.setAttribute("user", name);
						result = 0;
					} else {
						result = 3;
					}
				} else {
					result = 1;
				}
			}
		} catch (NamingException | SQLException e) {			
			e.printStackTrace();
			result = 2;
		} finally {
			if (rs != null) {
				try {
					rs.close();
				} catch (SQLException e) {
				}
			}
			if (stm != null) {
				try {
					stm.close();
				} catch (SQLException e) {
				}
			}
			if (con != null) {
				try {
					con.close();
				} catch (SQLException e) {
				}
			}
		}
		return result;
	}
}
