package fp.daw.classicmodels;

import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.sql.DataSource;

@WebServlet("/confirmar")
public class ConfirmServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		HttpSession session = request.getSession();
		if (session.getAttribute("usuario") != null)
			response.sendRedirect("catalogo");
		else {
			String codigoConfirmacion = request.getParameter("cc");
			if (codigoConfirmacion == null)
				response.sendRedirect("login.jsp?status=4");
			else {
				switch (confirm(codigoConfirmacion)) {
				case 0:
					response.sendRedirect("login.jsp?status=3");
					break;
				case 1:
					response.sendRedirect("login.jsp?status=4");
					break;
				case 2:
					response.sendRedirect("login.jsp?status=2");
				}
			}
		}
	}

	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		doGet(request, response);
	}

	private int confirm(String codigoConfirmacion) {
		Connection con = null;
		Statement stm = null;
		PreparedStatement pstm = null;
		ResultSet rs = null;
		int status = 0;
		try {
			Context context = new InitialContext();
			DataSource ds = (DataSource) context.lookup("java:comp/env/jdbc/classicmodels");
			con = ds.getConnection();
			stm = con.createStatement();
			String query = "select customerEmail from signups where confirmationCode = '" + codigoConfirmacion + "'";
			rs = stm.executeQuery(query);
			if (rs.next() == false) {
				status = 1;
			} else {
				pstm = con.prepareStatement("update signups set confirmationCode = NULL where confirmationCode = ?");
				pstm.setString(1, codigoConfirmacion);
				pstm.execute();			
			}
		} catch (NamingException | SQLException e) {			
			e.printStackTrace();
			status = 2;
		} finally {
			if (rs != null) {
				try {
					rs.close();
				} catch (SQLException e) {
				}
			}
			if (stm != null) {
				try {
					stm.close();
				} catch (SQLException e) {
				}
			}
			if (pstm != null) {
				try {
					pstm.close();
				} catch (SQLException e) {
				}
			}
			if (con != null) {
				try {
					con.close();
				} catch (SQLException e) {
				}
			}
		}
		return status;
	}
}
