package fp.daw.classicmodels;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

@WebServlet("/reenvio")
public class ForwardingServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		HttpSession session = request.getSession();
		if (session.getAttribute("usuario") != null)
			response.sendRedirect("catalogo");
		else {
			String nombre = request.getParameter("nombre");
			String email = request.getParameter("email");
			if (nombre == null|| email == null)
				response.sendRedirect("postregistro.jsp");
			else {
				sendCode(nombre, email);
			}
		}
	}

	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		doGet(request, response);
	}

	private void sendCode(String firstName, String email) {
		try {
			String confirmationCode = HashUtils.getBase64Digest(email);
			MailService.sendConfirmmationMessage(firstName, email, confirmationCode);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
