<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8" session="true"%>
<%
if (session.getAttribute("user") != null)
	response.sendRedirect("catalogo");
else {
	String firstName = request.getParameter("nombre");
	String lastName = request.getParameter("apellidos");
	String email = request.getParameter("email");
	if (firstName == null || lastName == null || email == null)
		response.sendRedirect("catalogo");
	else {
%>
<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
	<title>Registro</title>
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/css/bootstrap.min.css" integrity="sha384-TX8t27EcRE3e/ihU7zmQxVncDAy5uIKz4rEkgIXeMed4M0jlfIDPvg6uqKI2xXr2" crossorigin="anonymous">
	<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.6.3/css/all.css" integrity="sha384-UHRtZLI+pbxtHCWp1t77Bi1L4ZtiqrqD80Kn4Z8NTSRyMA2Fd33n5dQ8lWUE00s/" crossorigin="anonymous">
	<link rel="stylesheet" href="css/estilos.css">
</head>
<body>
	<div class="container">
		<div class="jumbotron">
			<h1 class="display-4">Bienvenido <%=firstName%></h1>
			<p class="lead">
				Sólo queda un paso más para completar tu registro y tener acceso a
				todos nuestros Servicios, sigue las instrucciones que te hemos enviado
				a <strong><%=email%></strong>
			</p>
			<hr class="my-4">
			<p>Pulsa el botón de reenviar para que te volvamos a enviar el mensaje de confirmación:</p>
			<form action="reenvio" method="post">
				<input type="hidden" name="nombre" value="<%=firstName%>" />
				<input type="hidden" name="email" value="<%=email%>" />
				<button type="submit" class="btn btn-primary">Reenviar</button>
			</form>
			<p class="lead mt-3">
				<a href="login.jsp">Iniciar sesión</a> |
				<a href="catalogo.jsp">Página de inicio</a>
			</p>
		</div>
	</div>
    <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ho+j7jyWK8fNQe+A12Hb8AhRq26LrZ/JpcUGGOn+Y7RsweNrtN/tE3MoK7ZeZDyx" crossorigin="anonymous"></script>
</body>
</html>
<%
}
}
%>