<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1" session="true"%>
<%
	if (session.getAttribute("user") != null)
		response.sendRedirect("catalogo.jsp");
	else {		
%>
<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
	<title>Login</title>
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/css/bootstrap.min.css" integrity="sha384-TX8t27EcRE3e/ihU7zmQxVncDAy5uIKz4rEkgIXeMed4M0jlfIDPvg6uqKI2xXr2" crossorigin="anonymous">
	<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.6.3/css/all.css" integrity="sha384-UHRtZLI+pbxtHCWp1t77Bi1L4ZtiqrqD80Kn4Z8NTSRyMA2Fd33n5dQ8lWUE00s/" crossorigin="anonymous">
	<link rel="stylesheet" href="css/estilos.css">
</head>
<body>
	<div class="container h-100">
  		<div class="row justify-content-center align-self-center">
  			<div class="jumbotron">
				<form action="login" method="post">
					<h1 class="display-4">Login</h1>
					<hr class="my-4">
					<%
					String status = request.getParameter("status");
					if (status != null) {
						switch (Integer.valueOf(status)) {
						case 1:
							%>
							<div class="alert alert-danger" role="alert">Las credenciales no son v�lidas.</div>
							<%
							break;
						case 2:
							%>
							<div class="alert alert-warning" role="alert">Ha habido un problema con la base de datos.</div>
							<%
							break;
						case 3:
							%>
							<div class="alert alert-success" role="alert">Enhorabuena, te has registrado correctamente. Ya puedes hacer login.</div>
							<%
							break;
						case 4:
							%>
							<div class="alert alert-info" role="alert">El c�digo de confirmaci�n no es v�lido.</div>
							<%
							break;
						}
					}
					%>
					<div class="form-group">
						<label for="email">E-mail</label>
						<input type="email" class="form-control" name="email" required="required" placeholder="Introducir email"/>
					</div>
					<div class="form-group">
						<label for="password">Contrase�a</label>
						<input type="password" class="form-control" name="password" required="required" placeholder="Contrase�a"/>
					</div>
					<button type="submit" class="btn btn-primary">Login</button>
					<p class="lead mt-3"><a href="registro">Si no tienes cuenta, reg�strate aqu�.</a></p>
				</form>
			</div>
		</div>
	</div>
    <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ho+j7jyWK8fNQe+A12Hb8AhRq26LrZ/JpcUGGOn+Y7RsweNrtN/tE3MoK7ZeZDyx" crossorigin="anonymous"></script>
</body>
</html>
<% } %>