<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
    
<c:if test="${empty sessionScope.user}">
	<c:redirect url="login" />
</c:if>

<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
	<title>Carrito</title>
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/css/bootstrap.min.css" integrity="sha384-TX8t27EcRE3e/ihU7zmQxVncDAy5uIKz4rEkgIXeMed4M0jlfIDPvg6uqKI2xXr2" crossorigin="anonymous">
	<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.6.3/css/all.css" integrity="sha384-UHRtZLI+pbxtHCWp1t77Bi1L4ZtiqrqD80Kn4Z8NTSRyMA2Fd33n5dQ8lWUE00s/" crossorigin="anonymous">
	<link rel="stylesheet" href="css/estilos.css">
</head>
<body>
	<div class="container">
		<!-- Navegación -->
		<nav class="navbar navbar-expand-lg navbar-light bg-light">
		  <a class="navbar-brand" href="#"><strong>Carrito de la compra</strong></a>
		  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
		    <span class="navbar-toggler-icon"></span>
		  </button>
		  <div class="collapse navbar-collapse" id="navbarSupportedContent">
		    <ul class="navbar-nav mr-auto">
		    </ul>
		    <span class="navbar-text dropdown">
		    	<a class="nav-link dropdown-toggle" href="#" id="navbarDropdownMenuLink" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
          			<i class="fa fa-user-alt"></i> ${sessionScope.user}
        		</a>
				<div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
		          <a class="dropdown-item" href="logout">Log Out</a>
		        </div>		      
		    </span>
		  </div>
		</nav>	

	<c:choose>
		<c:when test="${empty sessionScope.cart}">
			<p>No se han añadido productos al carrito</p>
		</c:when>
		<c:otherwise>
			<c:set var="totalPrice" value="0"/>
			<table class="table table-hover">
				<thead>
					<tr>
						<th>Producto</th>
						<th>Unidades</th>
						<th>Precio</th>
						<th>Importe</th>
						<th>Acciones</th>
					</tr>
				</thead>
				<tbody>				
				<c:forEach var="linea" items="${sessionScope.cart}">
					<c:set var="totalPrice" value="${totalPrice + (linea.value.amount * linea.value.price)}" />
					<tr>
						<td>${linea.value.productName}</td>
						<td>${linea.value.amount}</td>
						<td>${linea.value.price} €</td>
						<td>${linea.value.amount * linea.value.price} €</td>
						<td>
							<a href="cart?c=${linea.value.productCode}&r=1" data-toggle="tooltip" title="Eliminar del carrito"><i class="fa fa-trash"></i></a>
							<a href="cart?c=${linea.value.productCode}&u=add" data-toggle="tooltip" title="Aumentar unidad"><i class="fa fa-plus-square"></i></a>
							<a href="cart?c=${linea.value.productCode}&u=remove" data-toggle="tooltip" title="Reducir unidad"><i class="fa fa-minus-square"></i></a>
						</td>
					</tr>
				</c:forEach>
				</tbody>
				<tfoot>
					<tr>
						<td colspan="3"><strong>Total</strong></td>
						<td><strong>${totalPrice} €</strong></td>
						<td></td>
					</tr>
				</tfoot>
			</table>
			
			<a href="comprar.jsp" class="btn btn-primary btn-lg btn-block"><i class="fa fa-shopping-bag mr-2"></i>Comprar ahora</a>
			<a href="catalogo.jsp" class="btn btn-secondary btn-lg btn-block"><i class="fa fa-arrow-circle-left mr-2"></i>Volver al catálogo</a>
		</c:otherwise>		
	</c:choose>
	</div>
    <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ho+j7jyWK8fNQe+A12Hb8AhRq26LrZ/JpcUGGOn+Y7RsweNrtN/tE3MoK7ZeZDyx" crossorigin="anonymous"></script>
    <script>
	    $(function () {
    	  $('[data-toggle="tooltip"]').tooltip()
    	})
    </script>
</body>
</html>
