<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8" session="true"%>
<%
if (session.getAttribute("user") != null)
	response.sendRedirect("catalogo");
else {
%>
<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
	<title>Registro de usuario</title>
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/css/bootstrap.min.css" integrity="sha384-TX8t27EcRE3e/ihU7zmQxVncDAy5uIKz4rEkgIXeMed4M0jlfIDPvg6uqKI2xXr2" crossorigin="anonymous">
	<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.6.3/css/all.css" integrity="sha384-UHRtZLI+pbxtHCWp1t77Bi1L4ZtiqrqD80Kn4Z8NTSRyMA2Fd33n5dQ8lWUE00s/" crossorigin="anonymous">
	<link rel="stylesheet" href="css/estilos.css">
<script>
	function check(confirm) {
		if (confirm.value != document.getElementById('password').value) {
			confirm.setCustomValidity('Las contraseñas no coinciden');
		} else
			confirm.setCustomValidity('');
	}
</script>
</head>
<body>
	<div class="container h-100">
  		<div class="row justify-content-center align-self-center">
  			<div class="jumbotron">
				<form action="registro" method="post" onsubmit="check()">
					<h1 class="display-4">Registro de usuario</h1>
					<hr class="my-4">
					<%
					String status = request.getParameter("status");
					if (status != null) {
						switch (Integer.valueOf(status)) {
						case 1:
							%>
							<div class="alert alert-danger" role="alert">Ya existe un usuario registrado con esa dirección de email.</div>
							<%
							break;
						case 2:
							%>
							<div class="alert alert-warning" role="alert">Ha habido un problema al registrar el usuario.</div>
							<%
							break;
						}
					}
					%>
					<div class="form-group">
						<label for="nombre">Nombre</label>
						<input type="text" class="form-control" name="nombre" required="required" placeholder="Nombre"/>
					</div>
					<div class="form-group">
						<label for="apellidos">Apellidos</label>
						<input type="text" class="form-control" name="apellidos" required="required" placeholder="Apellidos"/>
					</div>
					<div class="form-group">
						<label for="email">E-mail</label>
						<input type="email" class="form-control" name="email" required="required" placeholder="Introducir email"/>
					</div>
					<div class="form-group">
						<label for="password">Contraseña</label>
						<input type="password" class="form-control" name="password" id="password" required="required" placeholder="Contraseña"/>
					</div>
					<div class="form-group">
						<label for="password">Confirmar contraseña</label>
						<input type="password" class="form-control" id="confirm" required="required" oninput="check(this)" placeholder="Repetir la contraseña" />
					</div>
					<button type="submit" class="btn btn-primary">Registrar</button>
				</form>
			</div>
		</div>
	</div>
    <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ho+j7jyWK8fNQe+A12Hb8AhRq26LrZ/JpcUGGOn+Y7RsweNrtN/tE3MoK7ZeZDyx" crossorigin="anonymous"></script>
</body>
</html>
<%
}
%>
