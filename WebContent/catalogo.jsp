<%@ taglib uri="http://java.sun.com/jsp/jstl/sql" prefix="sql"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8" session="true"%>

<%
	String currentPage = request.getParameter("page");
	if (currentPage == null)
		currentPage = "1";
	request.setAttribute("currentPage", currentPage);
%>
<sql:query var="productsCount" dataSource="jdbc/classicmodels">
    SELECT count(productCode) FROM products;
</sql:query>
<c:set var="total" value="${productsCount.rowsByIndex[0][0]}"/>
<sql:query var="modelos" dataSource="jdbc/classicmodels" startRow="${currentPage}" maxRows="9">
 select productCode, productName, productScale, productDescription, MSRP from products;
</sql:query>

<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
	<title>Catálogo de Productos</title>
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/css/bootstrap.min.css" integrity="sha384-TX8t27EcRE3e/ihU7zmQxVncDAy5uIKz4rEkgIXeMed4M0jlfIDPvg6uqKI2xXr2" crossorigin="anonymous">
	<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.6.3/css/all.css" integrity="sha384-UHRtZLI+pbxtHCWp1t77Bi1L4ZtiqrqD80Kn4Z8NTSRyMA2Fd33n5dQ8lWUE00s/" crossorigin="anonymous">
	<link rel="stylesheet" href="css/estilos.css">
</head>
<body>
	<div class="container">
		<!-- Navegación -->
		<nav class="navbar navbar-expand-lg navbar-light bg-light">
		  <a class="navbar-brand" href="catalogo.jsp"><strong>Catálogo de Modelos a Escala</strong></a>
		  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
		    <span class="navbar-toggler-icon"></span>
		  </button>
		  <div class="collapse navbar-collapse" id="navbarSupportedContent">
		    <ul class="navbar-nav mr-auto">
				<c:if test="${empty sessionScope.user}">
			      <li class="nav-item">
			        <a class="nav-link" href="login">Login</a>
			      </li>
				</c:if>
		    </ul>
			<c:if test="${not empty sessionScope.user}">
				<span class="mr-3">
					<a class="btn btn-primary" href="carrito.jsp"><i class="fa fa-shopping-cart"></i> Carrito 
						<c:if test="${not empty sessionScope.cart}">
							<span class="badge badge-light">${sessionScope.cart.size()}</span>
						</c:if>
					</a>
				</span>
			    <span class="navbar-text dropdown">
			    	<a class="nav-link dropdown-toggle" href="#" id="navbarDropdownMenuLink" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
	          			<i class="fa fa-user-alt"></i> ${sessionScope.user}
	        		</a>
					<div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
			          <a class="dropdown-item" href="logout">Log Out</a>
			        </div>		      
			    </span>
		    </c:if>
		  </div>
		</nav>	
	
		<c:if test="${not empty sessionScope.user}">
			<c:set var="pages" value="${total / 9}" />
			<p class="mt-3">Se han encontrado ${total} modelos</p>
			
			<!-- Paginación superior -->
			<nav>
				<ul class="pagination">
				<c:forEach begin="1" end="${pages}" varStatus="loop">
					<c:if test="${loop.index == currentPage}">
						<li class="page-item active">
							<a class="page-link" href="#">${loop.index}</a>
						</li>
					</c:if>
					<c:if test="${loop.index != currentPage}">
						<li class="page-item">
						    <a class="page-link" href="catalogo.jsp?page=${loop.index}">${loop.index}</a>
						</li> 
					</c:if>
				</c:forEach>
				</ul>
			</nav>
			
			<!-- Listado de modelos -->
			<c:forEach var="modelo" items="${modelos.rows}" varStatus="loop">
				<c:if test="${loop.index % 3 == 0}">
					<div class="card-deck">
				</c:if>
				<div class="card m-3">
					<div class="card-body">
						<h5 class="card-title"><c:out value="${modelo.productName}" /></h5>
						<h6 class="card-subtitle mb-2 text-muted">Escala <c:out value="${modelo.productScale}" /></h6>
						<p class="card-text"><c:out value="${modelo.productDescription}" /></p>
						<p class="card-text">Precio: <strong>${modelo.MSRP} €</strong>
						<c:if test="${not empty sessionScope.user}">
							<a class="card-link" href="cart?c=${modelo.productCode}&n=${modelo.productName}&p=${modelo.MSRP}&page=${currentPage}">
								<i class="fa fa-cart-plus"></i>
							</a>
						</c:if>
						</p>
					</div>
				</div>
				<c:if test="${(loop.index + 1) % 3 == 0}">
					</div>
				</c:if>
			</c:forEach>
		
			<!-- Paginación inferior -->
			<nav>
				<ul class="pagination">
				<c:forEach begin="1" end="${pages}" varStatus="loop">
					<c:if test="${loop.index == currentPage}">
						<li class="page-item active">
							<a class="page-link" href="#">${loop.index}</a>
						</li>
					</c:if>
					<c:if test="${loop.index != currentPage}">
						<li class="page-item">
						    <a class="page-link" href="catalogo.jsp?page=${loop.index}">${loop.index}</a>
						</li> 
					</c:if>
				</c:forEach>
				</ul>
			</nav>
		</c:if>
	</div>
    <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ho+j7jyWK8fNQe+A12Hb8AhRq26LrZ/JpcUGGOn+Y7RsweNrtN/tE3MoK7ZeZDyx" crossorigin="anonymous"></script>
</body>
</html>
